function check() {
    var a1 = document.getElementById("t1");
    var b1 = document.getElementById("t2");
    var c1 = document.getElementById("t3");
    var d1 = document.getElementById("t4");
    var e1 = document.getElementById("t5");
    var f1 = document.getElementById("t6");
    var g1 = document.getElementById("t7");
    var h1 = document.getElementById("t8");
    var a = parseFloat(a1.value);
    var b = parseFloat(b1.value);
    var c = parseFloat(c1.value);
    var d = parseFloat(d1.value);
    var e = parseFloat(e1.value);
    var f = parseFloat(f1.value);
    var g = parseFloat(g1.value);
    var h = parseFloat(h1.value);
    var i = Math.sqrt(((c - a) * (c - a)) + ((d - b) * (d - b)));
    var j = Math.sqrt(((c - e) * (c - e)) + ((d - f) * (d - f)));
    var k = Math.sqrt(((a - e) * (a - e)) + ((b - f) * (b - f)));
    var l = Math.sqrt(((c - g) * (c - g)) + ((d - h) * (d - h)));
    var m = Math.sqrt(((g - e) * (g - e)) + ((h - f) * (h - f)));
    var n = Math.sqrt(((a - g) * (a - g)) + ((b - h) * (b - h)));
    var s1 = (i + j + k) / 2;
    var s2 = (l + j + m) / 2;
    var s3 = (m + n + k) / 2;
    var s4 = (i + l + n) / 2;
    var z1 = Math.sqrt(s1 * (s1 - i) * (s1 - j) * (s1 - k));
    var z2 = Math.sqrt(s2 * (s2 - l) * (s2 - j) * (s2 - m));
    var z3 = Math.sqrt(s3 * (s3 - m) * (s3 - n) * (s3 - k));
    var z4 = Math.sqrt(s4 * (s4 - i) * (s4 - l) * (s4 - n));
    if (z1 - (z2 + z3 + z4) < 0.000001) {
        document.getElementById("p1").innerHTML = "The point P is inside the triangle.";
    }
    else {
        document.getElementById("p1").innerHTML = "The point P is outside the triangle";
    }
}
