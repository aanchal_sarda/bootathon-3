# Experiment 1
## ADDER CIRCUIT
> - ### AIM
The objective of this experiment is to <b>design and analyze</b> adder circuits. 
        
> - ### THEORY

> #### HALF ADDER

![img 1](img1.png)

A half adder adds two 1-bit binary numbers A and B to generate a 1-bit SUM (S) and a 1-bit CARRY (C) as output. 
The carry is theoretically carried on to the next bit position. The final sum numerically equals 2C + S.
The simplest half-adder design, shown in the picture, incorporates an XOR gate for S and an AND gate for C.
Half adders cannot be composed to produce larger bit adders as they lack a carry-in input.
The outputs S and C can be expressed as logical functions of input variables A,B as follows:
<br>
<b>
S = 1 either if (A = 0 and B = 1) or if (A = 1 and B = 0) = A XOR B
C= 1 only if (A = 1 and B = 1) = A AND B</b>

THE TRUTH TABLE FOR HALF ADDER CIRCUIT IS AS SHOWN.
| input1 | input2 | carry| sum |
| ----| ------ |--------|-------|
|0|0|0|0|
|0|1|0|1|
|1|0|0|1|
|1|1|1|0|

> #### FULL ADDER

![img 2](img2.png)

A full adder adds two 1-bit binary numbers along with a carry brought in and produces a sum and carry out as ouputs.
1-bit full adder adds three 1-bit numbers, often written as A, B,and Cin, where A and B are the operands, and Cin is a bit carried
in from a âpastâ addition.Circuit produces a 2-bit output sum typically represented by the signals Cout and S,
where the sum numerically equals 2Cout + S . 
A full adder can be implemented in many different ways using custom transistor-level circuits or using other gates.
<br>
<b>S = A XOR B XOR Cin
Cout = (A AND B) OR (Cin AND (A XOR B))</b>

THE TRUTH TABLE FOR HALF FULL CIRCUIT IS AS SHOWN.

|a|b|Cin|Cout|s|
|----|----|----|----|----|
|0|0|0|0|0|
|1|0|0|0|1|
|0|1|0|0|1|
|1|1|0|1|0|
|0|0|1|0|1|
|1|0|1|1|0|
|0|1|1|1|0|
|1|1|1|1|1|       
        
> - ### PROCEDURE
<ol>
                <li>
                    First, make a 1-bit half-adder circuit and test it with different inputs. 
                    You should also try giving time-varying waves as inputs and anlyze the change in the outputs. 
                    Read the manual page carefully to learn how this can be done.
                </li>
                <li>
                    Next, design 1-bit full adder circuit and test it out similarly. Feel free to try other implementations of full-adder circuits.
                </li>
                <li>
                    Save the full adder circuit. You can now load it as a block and connect them to make larger adders.
                </li>
                <li>
                    Make a 4-bit adder by connecting four full adders. Study its output for different kinds of inputs.
                </li>
                <li>
                    There is an import feature, which lets you construct building blocks. 
                    Click on import and then click on circuit board to see the imported block. For more details, refer import section in manual.
                </li>
</ol>
        
        
<b>
                NOTE: For referring to the manual and to virtualluy demonstrate the experiment visit the official page of vlabs.The link for 
                the same is provided below.All you need to do is click on it and it will take you to the experiment.
</b>
<br>
            

[Vlabs](http://cse15-iiith.vlabs.ac.in/ "Click to view labs")
    
        
    

